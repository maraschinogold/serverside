<?php

//Init session params
session_start();
//Enable debug tracking
error_reporting(E_ALL);
ini_set('display_errors', 1);


//If the user has an improper session
if (isset($_SESSION['CID']))
{
    echo json_encode("User already logged in");
    return;
}
if(isset($_POST['username'], $_POST['authtoken'], $_POST['email']))
{

    if(strlen($_POST['username']) < 3)
    {
        echo json_encode("ERROR: Username too short");
        return;
    }
    if(strlen($_POST['authtoken']) < 6)
    {
        echo json_encode("ERROR: Password too short");
        return;
    }

//SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

//Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) {
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 
    $prepared = $connectionInfo->prepare("INSERT INTO Contractors (Username, PasswordHash, Email) VALUES (?,?,?)");
    $prepared->bind_param("sss", $user, $pass, $email);

    $user  = $_POST['username'];
    $pass  = hash('sha256', $_POST['authtoken']);
    $email = $_POST['email'];

//Run it!
    $success = $prepared->execute();


    if($success == FALSE)
    {
        echo json_encode("Error: " . $prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }
    $prepared->close();
    //Extract CID from newly created line, as it is autoincremeneted. 
    $prepared = $connectionInfo->prepare("SELECT CID FROM Contractors WHERE Username = ?");
    $prepared->bind_param("s", $user);
    $success = $prepared->execute();

    if($success)
    {
        $result = $prepared->get_result();
        $row = $result->fetch_array(MYSQLI_NUM);
        $_SESSION['CID'] = $row[0];
        $_SESSION['user'] = $user;
        echo json_encode("Successfully registered and logged in");
    }
    $prepared->close();
    $connectionInfo->close();
    return;


}
else
{
    echo json_encode("Error: " . $prepared->error);
    $prepared->close();
    $connectionInfo->close();
    return;
}


echo json_encode("Incorrect fields sent");
return;

?>
