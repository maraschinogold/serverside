<?php
//Init session params
session_start();
//Enable debug tracking
error_reporting(E_ALL);
ini_set('display_errors', 1);

//If the user has an improper session
if (isset($_SESSION['CID']))
{
    echo json_encode("User already logged in");
    return;
}


if(isset($_POST['username'], $_POST['authtoken']))
{

    //SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    //Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) {
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 
    //Check for pass/user match
    $prepared = $connectionInfo->prepare("SELECT CID FROM Contractors WHERE Username = ? AND PasswordHash = ?");
    $prepared->bind_param("ss", $user, $pass);
    $user = $_POST['username'];
    $pass = hash('sha256', $_POST['authtoken']);

//Run it!
    $success = $prepared->execute();
    if($success == FALSE)
    {
        echo json_encode($prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }
//Extract mysqli result object
    $result = $prepared->get_result();
    $row = $result->fetch_array(MYSQLI_NUM);
    if($row[0] == "") //If no result or empty result
    {
        echo json_encode("Fail");
        $prepared->close();
        return;
    }
    else
    {
        $_SESSION['CID'] = $row[0];
        $_SESSION['user'] = $user;
        echo json_encode("Success");
        $prepared->close();
        return;

    }
}
else
{
    //This should only happen given bad POST request
    echo json_encode("Bad request");
}

return;




?>
