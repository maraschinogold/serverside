<?php


session_start();
if(isset($_SESSION['CID']) == FALSE)
{
    echo json_encode("User not logged in");
    return;
}
if(isset($_POST['username'], $_POST['authtoken'], $_POST['authtokenNew']) == FALSE)
{
    echo json_encode("Invalid Post Params");
    return;
}

$servername = "mysql.cs.orst.edu";
$username = "cs340_millardd";
$password = "6050";
$dbname = "cs340_millardd";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$prepared = $conn->prepare("UPDATE Contractors SET PasswordHash=? WHERE CID=? AND PasswordHash = ?");
$prepared->bind_param("iss", $CID, $old, $new);
$CID = $_SESSION['CID'];
$old  = hash('sha256', $_POST['authtoken']);
$new  = hash('sha256', $_POST['authtokenNew']);
$success = $prepared->execute();

if ($success === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

$conn->close();
?> 
