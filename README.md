# API Settings:
Master API URL: # 
https://web.engr.oregonstate.edu/~millardd/maraschinoGold/api.php

# **Login:** #
* feature:   "login"
* username:  string
* authtoken: string

# **Logout:** #
* feature:   "logout"

# **Register:** #
* feature:   "register"
* username:  string
* authtoken: string
* email:     string

# **List Projects:** #
* feature:   "listProjects"

# **List Roads:** #
* feature:   "listRoads"
* projectName: string

# **List Full Road:** #
* feature:   "queryRoad"
* roadName:  string 

# **Update Password:** #
* feature:      "updatePass"
* username:     string
* authtoken:    string
* authtokenNew: string
