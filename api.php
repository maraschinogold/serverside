<?php

//Init session params
session_start();
//Enable debug tracking
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_POST['feature']) == FALSE)
{
    echo json_encode("ERROR:  No function selected.");
    return;
}
$feature = $_POST['feature'];

if($feature == "login")
{

    if (isset($_SESSION['CID']))
    {
        echo json_encode("User already logged in");
        return;
    }


    if(isset($_POST['username'], $_POST['authtoken']))
    {

        //SQL database info
        $servername = "mysql.cs.orst.edu";
        $username = "cs340_millardd";
        $password = "6050";
        $dbname = "cs340_millardd";

        //Connect!
        $connectionInfo = new mysqli($servername, $username, $password, $dbname);
        if ($connectionInfo->connect_error) {
            echo json_encode("Connection failed: " . $connectionInfo->connect_error);
            return;
        } 
        //Check for pass/user match
        $prepared = $connectionInfo->prepare("SELECT CID FROM Contractors WHERE Username = ? AND PasswordHash = ?");
        $prepared->bind_param("ss", $user, $pass);
        $user = $_POST['username'];
        $pass = hash('sha256', $_POST['authtoken']);

        //Run it!
        $success = $prepared->execute();
        if($success == FALSE)
        {
            echo json_encode($prepared->error);
            $prepared->close();
            $connectionInfo->close();
            return;
        }
        //Extract mysqli result object
        $result = $prepared->get_result();
        $row = $result->fetch_array(MYSQLI_NUM);
        if($row[0] == "") //If no result or empty result
        {
            echo json_encode("Fail");
            $prepared->close();
            return;
        }
        else
        {
            $_SESSION['CID'] = $row[0];
            $_SESSION['user'] = $user;
            echo json_encode("Success");
            $prepared->close();
            return;

        }
    }
    else
    {
        //This should only happen given bad POST request
        echo json_encode("Bad request");
    }

    return;
}
if($feature == "logout")
{
    // Code pulled from PHP Manual session_destroy man page
    // http://php.net/session_destroy
    // Adapted boiler plate code as bandaide solution. 
    // echo was added to fit project spec.

    // Unset all of the session variables
    $_SESSION = array();

    // If it's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
                );
    }

    // Finally, destroy the session.
    session_destroy();

    echo json_encode("Logout successful");
    return;

}
if($feature == "listProjects")
{
    //If the user has an improper session
    if(isset($_SESSION['user'], $_SESSION['CID']) == FALSE)
    {
        echo json_encode("User not logged in!");
        return;
    }

    //SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    //Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) { //If it failed...
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 

    //Select Jobsite titles from Roster view
    $prepared = $connectionInfo->prepare("SELECT Title FROM Roster WHERE Username = ?");
    $prepared->bind_param("s", $user);
    $user = $_SESSION['user'];

    //Run it!
    $success = $prepared->execute();
    if($success == FALSE)
    {
        echo json_encode($prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }

    //Extract mysqli result object
    $result = $prepared->get_result();

    while($row = $result->fetch_array(MYSQLI_NUM)) //Extract contents
    {
        $final[] = $row; //Stash array for transport
    }
    if(isset($final))
        echo json_encode($final); //Send it back up!
    else
        echo json_encode("No results");
    return;

}
if($feature == "register")
{

    //If the user has an improper session
    if (isset($_SESSION['CID']))
    {
        echo json_encode("User already logged in");
        return;
    }
    if(isset($_POST['username'], $_POST['authtoken'], $_POST['email']))
    {

        if(strlen($_POST['username']) < 3)
        {
            echo json_encode("ERROR: Username too short");
            return;
        }
        if(strlen($_POST['authtoken']) < 6)
        {
            echo json_encode("ERROR: Password too short");
            return;
        }

        //SQL database info
        $servername = "mysql.cs.orst.edu";
        $username = "cs340_millardd";
        $password = "6050";
        $dbname = "cs340_millardd";

        //Connect!
        $connectionInfo = new mysqli($servername, $username, $password, $dbname);
        if ($connectionInfo->connect_error) {
            echo json_encode("Connection failed: " . $connectionInfo->connect_error);
            return;
        } 
        $prepared = $connectionInfo->prepare("INSERT INTO Contractors (Username, PasswordHash, Email) VALUES (?,?,?)");
        $prepared->bind_param("sss", $user, $pass, $email);

        $user  = $_POST['username'];
        $pass  = hash('sha256', $_POST['authtoken']);
        $email = $_POST['email'];

        //Run it!
        $success = $prepared->execute();


        if($success == FALSE)
        {
            echo json_encode("Error: " . $prepared->error);
            $prepared->close();
            $connectionInfo->close();
            return;
        }
        $prepared->close();
        //Extract CID from newly created line, as it is autoincremeneted. 
        $prepared = $connectionInfo->prepare("SELECT CID FROM Contractors WHERE Username = ?");
        $prepared->bind_param("s", $user);
        $success = $prepared->execute();

        if($success)
        {
            $result = $prepared->get_result();
            $row = $result->fetch_array(MYSQLI_NUM);
            $_SESSION['CID'] = $row[0];
            $_SESSION['user'] = $user;
            echo json_encode("Successfully registered and logged in");
        }
        $prepared->close();
        $connectionInfo->close();
        return;


    }
    else
    {
        echo json_encode("Error: " . $prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }


    echo json_encode("Incorrect fields sent");
    return;

}
if($feature == "updatePass")
{


    if(isset($_SESSION['CID']) == FALSE)
    {
        echo json_encode("User not logged in");
        return;
    }
    if(isset($_POST['username'], $_POST['authtoken'], $_POST['authtokenNew']) == FALSE)
    {
        echo json_encode("Invalid Post Params");
        return;
    }

    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $prepared = $conn->prepare("UPDATE Contractors SET PasswordHash=? WHERE CID=? AND PasswordHash = ?");
    $prepared->bind_param("iss", $CID, $old, $new);
    $CID = $_SESSION['CID'];
    $old  = hash('sha256', $_POST['authtoken']);
    $new  = hash('sha256', $_POST['authtokenNew']);
    $success = $prepared->execute();

    if ($success === TRUE) {
        echo json_encode("Record updated successfully");
    } else {
        echo json_encode("Error updating record: " . $conn->error);
    }

    $conn->close();
}


if($feature == "listRoads")
{

    //If the user has an improper session
    if(isset($_SESSION['user'], $_SESSION['CID']) == FALSE)
    {
        echo json_encode("User not logged in!");
        return;
    }
    if(isset($_POST['projectName']) == FALSE)
    {
        echo json_encode("Bad POST param...");
        return;
    }
    //SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    //Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) { //If it failed...
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 

    //Select Jobsite titles from Roster view
    $prepared = $connectionInfo->prepare("SELECT Name, RequiredTime FROM Roads JOIN Project ON Roads.PID = Project.PID WHERE Title = ?");
    $prepared->bind_param("s", $name);
    $name = $_POST['projectName'];

    //Run it!
    $success = $prepared->execute();
    if($success == FALSE)
    {
        echo json_encode("ERROR: " .$prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }

    //Extract mysqli result object
    $result = $prepared->get_result();

    while($row = $result->fetch_array(MYSQLI_NUM)) //Extract contents
    {
        $final[] = $row; //Stash array for transport
    }
    if(isset($final))
        echo json_encode($final); //Send it back up!
    else
        echo json_encode("No results found");
    return;


}

if($feature == "addRoad")
{

    if(isset($_POST['name']) == FALSE)
    {
        echo json_encode("Missing param");
        return;
    }
    if(isset($_POST['pid']) == FALSE)
    {
        echo json_encode("Missing param");
        return;
    }
    if(isset($_POST['sprayRate']) == FALSE)
    {
        echo json_encode("Missing param");
        return;
    }
    if(isset($_POST['temp']) == FALSE)
    {
        echo json_encode("Missing param");
        return;
    }


    //SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    //Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) {
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 

    $prepared = $connectionInfo->prepare("INSERT INTO cs340_millardd.Roads (Name, PID, EID, RequiredTime, SprayRate, Temp) VALUES (?, ?, 0, 0, ?, ?)");
    $prepared->bind_param("sidd", $name, $pid, $spray, $temp);

    $name = $_POST['name'];
    $pid  = $_POST['pid'];
    $spray= $_POST['sprayRate'];
    $temp = $_POST['temp'];

    //Run it!
    $success = $prepared->execute();


    if($success == FALSE)
    {
        echo json_encode("Error: " . $prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }
    $prepared->close();
    echo json_encode("Added...");
    $connectionInfo->close();
    return;


}
else
{
    echo json_encode("Error: " . $prepared->error);
    $prepared->close();
    $connectionInfo->close();
    return;
}

if($feature == "askRoad")
{

    //If the user has an improper session
    if(isset($_SESSION['user'], $_SESSION['CID']) == FALSE)
    {
        echo json_encode("User not logged in!");
        return;
    }
    if(isset($_POST['roadName']) == FALSE)
    {
        echo json_encode("Bad POST param...");
        return;
    }
    //SQL database info
    $servername = "mysql.cs.orst.edu";
    $username = "cs340_millardd";
    $password = "6050";
    $dbname = "cs340_millardd";

    //Connect!
    $connectionInfo = new mysqli($servername, $username, $password, $dbname);
    if ($connectionInfo->connect_error) { //If it failed...
        echo json_encode("Connection failed: " . $connectionInfo->connect_error);
        return;
    } 

    //Select Jobsite titles from Roster view
    $prepared = $connectionInfo->prepare("SELECT Name, PID, EID, RequiredTime, SprayRate, Temp FROM Roads WHERE Name = ?");
    $prepared->bind_param("s", $name);
    $name = $_POST['roadName'];

    //Run it!
    $success = $prepared->execute();
    if($success == FALSE)
    {
        echo json_encode("ERROR: " .$prepared->error);
        $prepared->close();
        $connectionInfo->close();
        return;
    }

    //Extract mysqli result object
    $result = $prepared->get_result();

    while($row = $result->fetch_array(MYSQLI_NUM)) //Extract contents
    {
        $final[] = $row; //Stash array for transport
    }
    if(isset($final))
        echo json_encode($final); //Send it back up!
    else
        echo json_encode("No results found");
    return;


}












echo json_encode("Incorrect fields sent");
return;



?>
