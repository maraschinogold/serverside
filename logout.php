<?php
// Initialize the session.
// Code pulled from PHP Manual session_destroy man page
// http://php.net/session_destroy

// Adapted boiler plate code as bandaide solution. 
// echo was added to fit project spec.
session_start();

// Unset all of the session variables.
$_SESSION = array();

// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finally, destroy the session.
session_destroy();

echo json_encode("Logout successful");
return;
?>
