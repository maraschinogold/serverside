<?php
//Init session params
session_start();
//Enable debug tracking
error_reporting(E_ALL);
ini_set('display_errors', 1);

//If the user has an improper session
if(isset($_SESSION['user'], $_SESSION['CID']) == FALSE)
{
    echo json_encode("User not logged in!");
    return;
}

//SQL database info
$servername = "mysql.cs.orst.edu";
$username = "cs340_millardd";
$password = "6050";
$dbname = "cs340_millardd";

//Connect!
$connectionInfo = new mysqli($servername, $username, $password, $dbname);
if ($connectionInfo->connect_error) { //If it failed...
    echo json_encode("Connection failed: " . $connectionInfo->connect_error);
    return;
} 

//Select Jobsite titles from Roster view
$prepared = $connectionInfo->prepare("SELECT Title FROM Roster WHERE Username = ?");
$prepared->bind_param("s", $user);
$user = $_SESSION['user'];

//Run it!
$success = $prepared->execute();
if($success == FALSE)
{
    echo json_encode($prepared->error);
    $prepared->close();
    $connectionInfo->close();
    return;
}

//Extract mysqli result object
$result = $prepared->get_result();

while($row = $result->fetch_array(MYSQLI_NUM)) //Extract contents
{
    $final[] = $row; //Stash array for transport
}

echo json_encode($final); //Send it back up!



?>
